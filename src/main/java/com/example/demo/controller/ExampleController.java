package com.example.demo.controller;

import com.example.demo.dto.HelloBeanDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
public class ExampleController {

    @Autowired
    private MessageSource messageSource;
    @GetMapping(path = "/welcome/{name}")
    public HelloBeanDto myName(@PathVariable String name){
        return new HelloBeanDto(String.format("Hello, my name is: %s", name));
    }

    @GetMapping(path = "/lang/all")
    public String myNameFrench(@RequestHeader(name = "Accept-Language", required = false) Locale locale, @PathVariable String name){
        return messageSource.getMessage("good.morning.message", null, locale);
    }
}
