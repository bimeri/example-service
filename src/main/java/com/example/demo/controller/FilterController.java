package com.example.demo.controller;

import com.example.demo.dto.NameDto;
import com.example.demo.dto.UserInfoDto;
import com.example.demo.service.UserDoaService;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FilterController {
    @Autowired
 private UserDoaService userDoaService;
    @GetMapping(path = "/userinfo")
    public UserInfoDto userInfo(){
        UserInfoDto info = new UserInfoDto(1,new NameDto("Bimeri", "Magaza"), "bimerinoel@gmail.com", "noelino");
        return info;
    }
    @GetMapping(path = "/userinfos")
    public List<UserInfoDto> userInfos(){
        return userDoaService.userInfos();
    }

    // filtering value using dynamic filtering
@GetMapping(path = "/userinfos/dynamic")
    public MappingJacksonValue dynamicInfo(){
        List<UserInfoDto> info = userDoaService.userInfos();
    SimpleBeanPropertyFilter simpleBeanPropertyFilter = SimpleBeanPropertyFilter.filterOutAllExcept(
            "fistName", "lastName", "email"
    );
    FilterProvider filterProvider = new SimpleFilterProvider().addFilter("userFilter", simpleBeanPropertyFilter);
    MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(info);
    mappingJacksonValue.setFilters(filterProvider);
        return mappingJacksonValue;
}

// versioning restful web service method two with request param and header
    @GetMapping(path = "/person/param", params = "version=1")
    public NameDto param1(){
        return new NameDto("name1", "name2");
    }

    @GetMapping(path = "/person/header", headers = "X-API-VERSION=1")
    public NameDto version1(){
        return new NameDto("name1", "name2");
    }
}
