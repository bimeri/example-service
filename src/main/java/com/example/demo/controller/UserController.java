package com.example.demo.controller;

import com.example.demo.dto.UserBeanDto;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.service.UserDoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserDoaService userDoaService;

    // get all users
    @GetMapping(path = "users")
    public List<UserBeanDto> allUsers() {
        return userDoaService.allUsers();
    }

    // get a specific user by id
    @GetMapping(path = "users/{id}")
    public UserBeanDto findOneUser(@PathVariable Integer id) {
        UserBeanDto user = userDoaService.getUser(id);
        if(user == null) {
            throw new UserNotFoundException("user_id: " + id);
        }
        // sending user detail with link to user
//        Resource<UserBeanDto> resource = new Resource<UserBeanDto>(user);
//        ControllerLinkBuilder lintTo = linkTo(methodOn(this.getClass()).retrieveAllUsers());
//        resource.add(linkTo.withRel("all-users"));
//        return resource;
        return user;
    }

    // add user
    @PostMapping(path = "users")
    public ResponseEntity<Object> addUser(@Valid @RequestBody UserBeanDto userBeanDto) {
        UserBeanDto user = userDoaService.addUser(userBeanDto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    // delete a user
    @DeleteMapping(path = "users/{id}")
    public ResponseEntity<Object> removeUser(@PathVariable Integer id) {
        UserBeanDto user = userDoaService.deleteUser(id);
        if(user == null)
            throw new UserNotFoundException("user_id: " + id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(path = "user/update")
    public List<UserBeanDto> updateUser(@RequestBody UserBeanDto userBeanDto) {
        userDoaService.updateUser(userBeanDto);
        return userDoaService.allUsers();
    }
}
