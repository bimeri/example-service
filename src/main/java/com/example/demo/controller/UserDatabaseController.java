package com.example.demo.controller;

import com.example.demo.exception.UserNotFoundException;
import com.example.demo.model.Post;
import com.example.demo.model.User;
import com.example.demo.repository.PostRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class UserDatabaseController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PostRepository postRepository;

    @GetMapping(path = "/data/users")
    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    @GetMapping(path = "/data/user/{id}")
    public Optional<User> getOneUser(@PathVariable Integer id){
        Optional<User> user = userRepository.findById(id);
        if(user.isPresent()){
            return user;
        } else {
            throw new UserNotFoundException("id- "+ id);
        }
    }

    @DeleteMapping(path = "/data/user/{id}")
    public void deleteOneUser(@PathVariable Integer id){
         userRepository.deleteById(id);
    }

    @PostMapping(path = "/data/users")
    public ResponseEntity<Object> createUser(@RequestBody User user){
         userRepository.save(user);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping(path = "/data/users/{id}/posts")
    public List<Post> getUserPosts(@PathVariable Integer id){
        Optional<User> user = userRepository.findById(id);
        if(!user.isPresent()){
            throw new UserNotFoundException("user-id: " + id);
        }
        return user.get().getPosts();
    }

    @PostMapping(path = "/data/users/{id}/posts")
    public void createPost(@RequestBody Post post, @PathVariable Integer id){
        Optional<User> user = userRepository.findById(id);
        if(!user.isPresent()){
            throw new UserNotFoundException("user-id: "+ id);
        }
        Post postObject = new Post();
        postObject.setTitle(post.getTitle());
        postObject.setDescription(post.getDescription());
        postObject.setUser(user.get());
        postRepository.save(postObject);

    }
}
