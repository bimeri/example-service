package com.example.demo.dto;

import lombok.Data;

@Data
public class HelloBeanDto {
    private String name;
    public HelloBeanDto(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return String.format("%s", name);
    }
}
