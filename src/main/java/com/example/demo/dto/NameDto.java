package com.example.demo.dto;

import lombok.Data;

@Data
public class NameDto {
    private String firstName;
    private String lastName;

    public NameDto(String firstName, String lastName){
        super();
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
