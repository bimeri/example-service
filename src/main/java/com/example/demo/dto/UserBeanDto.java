package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
public class UserBeanDto {
    private Integer id;

    @Size(min=3, max = 10, message = "name should be between 2 and 10 character")
    private String name;
    private Integer age;

    @Past
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birth;

    public UserBeanDto(Integer id, String name, Integer age, Date birth) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.birth = birth;
    }

    public String toString() {
        return String.format("id: %d, name: %s, age: %d, birth: %s", id, name, age, birth);
    }
}
