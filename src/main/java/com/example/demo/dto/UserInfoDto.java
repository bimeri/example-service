package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
//@JsonIgnoreProperties({"firstName", "email"})
@JsonFilter("userFilter")
public class UserInfoDto {
    private int userId;
    private  NameDto names;
    private String email;

    @JsonIgnore
    private String password;

    public UserInfoDto(int userId, NameDto names, String email, String password){
        super();
        this.userId = userId;
        this.email = email;
        this.password = password;
        this.names = names;
    }
}
