package com.example.demo.exception;

public enum ErrorCode {
    CHARACTER_LENGTH_ERROR("length mismatched"),
    METHOD_NOT_IMPLEMENTED("method not implemented");

    private String message;
     ErrorCode(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
