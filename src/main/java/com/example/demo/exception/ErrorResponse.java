package com.example.demo.exception;

import lombok.Data;

import java.util.Date;

@Data
public class ErrorResponse {
    private Date timestamp;
    private String code;
    private String message;
    private String endpoint;

    public ErrorResponse(Date timestamp, String code, String message, String endpoint){
        this.timestamp = timestamp;
        this.code = code;
        this.message = message;
        this.endpoint = endpoint;
    }
}
