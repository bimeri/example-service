package com.example.demo.model;

import com.example.demo.dto.NameDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.List;

@ApiModel(description = "all details about the user")
@Entity
@Data
public class User {
    @Id
    @GeneratedValue
    private int id;

    private String firstName;
    private String lastName;

    @JsonIgnore
    private String password;

    private String email;

    @OneToMany(mappedBy = "user")
    private List<Post> posts;

    @Past
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birth;

    public User(int id, String firstName, String lastName, String password, String email, Date birth){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.birth = birth;
    }
}
