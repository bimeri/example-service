package com.example.demo.service;

import com.example.demo.dto.NameDto;
import com.example.demo.dto.UserBeanDto;
import com.example.demo.dto.UserInfoDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
public class UserDoaService {
    private static List<UserBeanDto> users = new ArrayList<>();
    private static  List<UserInfoDto> userInfoDto = new ArrayList<>();

    static {
        userInfoDto.add(new UserInfoDto(1, new NameDto("f", "noel"), "val@gc.df", "12345"));
        userInfoDto.add(new UserInfoDto( 2, new NameDto("f", "noel"), "vnono@ff.com", "11111"));
        userInfoDto.add(new UserInfoDto(3,new NameDto("f", "noel"), "df@ds.dc", "1345"));
        userInfoDto.add(new UserInfoDto(4,new NameDto("f", "noel"), "df@ds.dc", "1345"));
    }

    static {
        users.add(new UserBeanDto(1, "Noel Magaza", 23, new Date()));
        users.add(new UserBeanDto(2, "Coffee Chuffe", 20, new Date()));
        users.add(new UserBeanDto(3, "Stephan Mokete", 22, new Date()));
        users.add(new UserBeanDto(4, "bosco John", 30, new Date()));
    }

    private static int countUsers = users.size();

    public UserBeanDto addUser(UserBeanDto userBeanDto) {
        if (userBeanDto.getId() == null) {
            userBeanDto.setId(++countUsers);
        }
        users.add(userBeanDto);
        return getUser(userBeanDto.getId());
    }

    public UserBeanDto getUser(Integer id) {
        for (UserBeanDto user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public List<UserBeanDto> allUsers() {
        return users;
    }

    public UserBeanDto deleteUser(Integer id) {
//        for (UserBeanDto user : users) {
//            if (user.getId() == id) {
//                users.remove(getUser(id));
//            }
//        }
        // or this other method also works
        Iterator<UserBeanDto> iterator = users.iterator();
        UserBeanDto user = iterator.next();
        while (iterator.hasNext()) {
            if(user.getId() == id){
                iterator.remove();
                return user;
            }
        }
        return null;
    }

    public void updateUser(UserBeanDto userr) {
        UserBeanDto user = getUser(userr.getId());
        user.setAge(userr.getAge());
        user.setBirth(userr.getBirth());
        user.setName(userr.getName());
    }

    public List<UserInfoDto> userInfos(){
        return userInfoDto;
    }
}
