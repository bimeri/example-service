package com.example.demo;

import com.example.demo.dto.HelloBeanDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ExampleControllerTest {

    @Test
    public void helloBeanTest(){
        HelloBeanDto bean = new HelloBeanDto("noel");
        Assertions.assertEquals(bean,bean);
    }
}
